#!/bin/bash

# For GPU refer below link
# https://huggingface.co/docs/text-generation-inference/en/quicktour

# For CPU refer below link
# https://huggingface.co/docs/text-generation-inference/en/installation_intel#using-tgi-with-intel-cpus

# For CPU use: ghcr.io/huggingface/text-generation-inference:2.3.1-intel-cpu
# For CPU use latest: ghcr.io/huggingface/text-generation-inference:latest-intel-cpu

model=HuggingFaceTB/SmolLM-135M
volume=$PWD/data # share a volume with the Docker container to avoid downloading weights every run
token=<HF_TOKEN>

# Check if the container with name 'hf_tgi' exists
if sudo docker ps -a --format "{{.Names}}" | grep -q "^hf_tgi$"; then
    # Start the container if it exists
    sudo docker start hf_tgi

    echo "Docker container by the name hf_tgi is up now."
else
    # Run the container if it does not exist
    sudo docker run -d \
        --shm-size 1g \
        -e HF_TOKEN=$token \
        -p 8080:80 \
        -v $volume:/data \
        --name hf_tgi \
        ghcr.io/huggingface/text-generation-inference:latest \
        --model-id $model \
        --json-output \
        --max-input-tokens 1024

    echo "Docker container by the name hf_tgi is up now."
fi

