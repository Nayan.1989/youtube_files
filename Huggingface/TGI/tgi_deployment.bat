@echo off
setlocal
set token=<HF_TOKEN>
set volume=%CD%\data
set model=HuggingFaceTB/SmolLM-135M

rem For GPU refer below link
rem https://huggingface.co/docs/text-generation-inference/en/quicktour

rem For CPU refer below link
rem https://huggingface.co/docs/text-generation-inference/en/installation_intel#using-tgi-with-intel-cpus

rem For CPU use: ghcr.io/huggingface/text-generation-inference:2.3.1-intel-cpu
rem For CPU use latest: ghcr.io/huggingface/text-generation-inference:latest-intel-cpu


rem Check if the container with name 'hf_tgi' exists
for /f "tokens=*" %%i in ('docker ps -a --format "{{.Names}}"') do (
    if "%%i"=="hf_tgi" set containerExists=1
)
echo %containerExists%

if defined containerExists (
    rem Start the container if it exists
    docker start hf_tgi
    echo "Docker container by the name hf_tgi is up now."
) else (
    rem Run the container if it does not exist
    docker run -d ^
        --shm-size 1g ^
        -e HF_TOKEN=%token% ^
        -p 8080:80 ^
        -v %volume%:/data ^
        --name hf_tgi ^
        ghcr.io/huggingface/text-generation-inference:latest ^
        --model-id %model% ^
        --json-output ^
        --max-input-tokens 1024

    echo "Docker container by the name hf_tgi is up now."
)

endlocal
