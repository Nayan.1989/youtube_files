import os
from datetime import datetime
from dotenv import load_dotenv
from transformers import AutoModelForCausalLM, AutoTokenizer, pipeline


print(f"Current working directory is {os.getcwd()}")

# Load environment variables from .env file available in same directory
print("Loading all env variables.....")
load_dotenv()

# access .env variables
print("Accessing HF Token.....")
token = os.getenv("hf_token")

# define model to be used
model_id = "HuggingFaceTB/SmolLM-135M"

# download the tokenizer and access it
print("Define tokenize.....")
tokenizer = AutoTokenizer.from_pretrained(
    model_id,
    cache_dir="models",
    token=token,
)

# download the model and access it
print("Define model.....")
model = AutoModelForCausalLM.from_pretrained(
    model_id,
    cache_dir="models",
    device_map="auto",
    offload_folder="offload",
    token=token,
)

# define pipeline for inference
print("Define pipeline.....")
pipe = pipeline(
    "text-generation",
    model=model,
    tokenizer=tokenizer,
    max_new_tokens=1024,  #  Specifies the maximum number of new tokens to generate, in addition to the input tokens.
)

print(f"Process start time is {datetime.now()}")
st = datetime.now()
print(pipe("Provide background on transformers."))
en = datetime.now()
print(f"Process end time is {datetime.now()}")
print(f"total time is {en-st}")
