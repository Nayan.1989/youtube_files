def sumation(a, b):
    """
    A function to sum 2 numbers.

    Parameters
    ----------
    a : int
        First number

    b : int
        Second number

    Returns
    ------
    return : int
        Sum of 2 numbers, i.e. a+b
    """
    print("sumation method has been called and sum is",a+b)
    return a+b