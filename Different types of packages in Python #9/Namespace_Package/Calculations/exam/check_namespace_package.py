import Calculations

# Namespace packages have no __file__ attribute.
# Below will provide None details it means its a namespace package
print("*"*100)
print("Namespace package without __file__ attribute having None value.")
print(Calculations.__file__)

# Namespace packages' __path__ attribute is a read-only iterable of strings,
# which is automatically updated when the parent path is modified.

# There is a prefix _NamespacePath in the result it means its a namespace package
print("\n")
print("Namespace package with __path__ attribute having _NamespacePath prefixed in the result.")
print(Calculations.__path__)

# Here in the result there is nothing mentioned as NamespaceLoader
# Its showing NamespaceLoader hence its a namespace package
print("\n")
print("Namespace package with __loader__ attribute having NamespaceLoader in the result.")
print(Calculations.__loader__)

# Namespace packages have no __init__.py module.
# But regular package will have.
