import sys

# path = "/home/nayan/Documents/Repository/Python/8. __init__ in Python Folders/"
# sys.path.insert(0, path)

print(sys.path)

"""
Below will work when run from Python Console of PyCharm

But it wont work if we execute below command
python Calculations/exam/examples.py or python examples.py
It will result in an error that module not found.
In that case we need to add sys.path.insert(path/of/Calculations/folder,0)
"""
from Calculations.addi.addition import sumation
from Calculations.mult.multiply import multiplication


if __name__ == "__main__":
    sumation(5, 9)
    multiplication(3, 9)


