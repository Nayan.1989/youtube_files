def multiplication(a, b):
    """
    A function to multiply 2 numbers.

    Parameters
    ----------
    a : int
        First number

    b : int
        Second number

    Returns
    ------
    return : int
        Multiplication of 2 numbers, i.e. a+b
    """
    print("multiplication method has been called and multiplication is", a * b)
    return a+b