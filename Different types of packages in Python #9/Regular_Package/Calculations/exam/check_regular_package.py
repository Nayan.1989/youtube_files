import Calculations

# Namespace packages have no __file__ attribute.
# But below will provide somde details it means its a regular package
print("*"*100)
print("Regular package with __file__ attribute having some value.")
print(Calculations.__file__)

# Namespace packages' __path__ attribute is a read-only iterable of strings,
# which is automatically updated when the parent path is modified.

# There is no prefix _NamespacePath in the result it means its a regular package.
print("\n")
print("Regular package with __path__ attribute having no _NamespacePath in the result.")
print(Calculations.__path__)

# Here in the result there is nothing mentioned as NamespaceLoader
# Its showing SourceFileLoader hence its a regular package
print("\n")
print("Regular package with __loader__ attribute having SourceFileLoader in the result.")
print(Calculations.__loader__)

# Namespace packages have no __init__.py module.
# But regular package will have.
