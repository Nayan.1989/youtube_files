"""
This script demonstrates univariate linear regression analysis.

Python==3.12.3
numpy==2.0.0
pandas==2.2.2
scikit-learn==1.5.1
"""


import numpy as np
import pandas as pd
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression

# Sample data
data = {
    "Salt_Intake": [1.2, 1.5, 1.8, 2.0, 2.3, 2.6, 2.9, 3.1, 3.4, 3.7, 4.0, 4.2, 4.5, 4.8, 5.0, 5.3, 5.6, 5.9, 6.1, 6.4,
                    6.7, 7.0, 7.2, 7.5, 7.8, 8.0, 8.3, 8.6, 8.9, 9.1, 9.4, 9.7, 10.0, 10.2, 10.5, 10.8, 11.1, 11.3, 11.6,
                    11.9, 12.1, 12.4, 12.7, 13.0, 13.2, 13.5, 13.8, 14.0, 14.3, 14.6, 14.9, 15.1, 15.4, 15.7, 16.0, 16.2,
                    16.5, 16.8, 17.0, 17.3, 17.6, 17.9, 18.1, 18.4, 18.7, 19.0, 19.2, 19.5, 19.8, 20.0, 20.3, 20.6, 20.9,
                    21.1, 21.4, 21.7, 22.0, 22.2, 22.5, 22.8, 23.0, 23.3, 23.6, 23.9, 24.1, 24.4, 24.7, 25.0, 25.2, 25.5,
                    25.8, 26.0, 26.3, 26.6, 26.9, 27.1, 27.4, 27.7, 28.0, 28.2],
    "Blood_Pressure": [110, 112, 113, 115, 116, 117, 119, 120, 121, 123, 124, 125, 127, 128, 129, 130, 132, 133, 134, 136,
                       137, 138, 140, 141, 142, 143, 145, 146, 147, 148, 150, 151, 152, 154, 155, 156, 158, 159, 160, 161,
                       163, 164, 165, 167, 168, 169, 170, 172, 173, 174, 176, 177, 178, 179, 181, 182, 183, 184, 186, 187,
                       188, 189, 191, 192, 193, 195, 196, 197, 198, 200, 201, 202, 204, 205, 206, 207, 209, 210, 211, 213,
                       214, 215, 216, 218, 219, 220, 221, 223, 224, 225, 226, 228, 229, 230, 232, 233, 234, 235, 237, 238]
}

# Create DataFrame
df = pd.DataFrame(data)

# Independent variable (Salt Intake)
X = df['Salt_Intake'].values.reshape(-1, 1)

# Dependent variable (Blood Pressure)
Y = df['Blood_Pressure'].values

# Create and fit the linear regression model
model = LinearRegression()
model.fit(X, Y)

# Predictions
Y_pred = model.predict(X)

# Calculate SSE
SSE = np.sum((Y - Y_pred) ** 2)

# Calculate SST
mean_Y = np.mean(Y)
SST = np.sum((Y - mean_Y) ** 2)

# Calculate R^2
R2 = r2_score(Y, Y_pred)

# Calculate Adjusted R^2
n = len(Y)
k = 1  # number of predictors
adj_R2 = 1 - ((1 - R2) * (n - 1) / (n - k - 1))

# Get the coefficients of the model
b0 = model.intercept_
b1 = model.coef_[0]

# Print the results
print("Predicted values are as below:")
print(Y_pred)
print(f"The linear regression equation is: Y = {b0:.2f} + {b1:.2f}X")
print(f"SSE: {SSE}")
print(f"SST: {SST}")
print(f"R^2: {R2}")
print(f"Adjusted R^2: {adj_R2}")
