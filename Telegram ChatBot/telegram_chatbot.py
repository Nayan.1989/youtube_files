"""
The telegram bot related code is taken from https://github.com/cmd410/OrigamiBot
and then modified with our LLM bot to have conversation with database.
"""

# Install required packages.
# ==================================================================================================
# python -V == 3.12.3
# pip install psycopg2==2.9.9
# pip install origamibot==2.3.6
# pip install sqlalchemy==2.0.31
# pip install llama-index==0.10.50
# pip install llama-index-llms-huggingface==0.2.4
# pip install llama-index-embeddings-huggingface==0.2.2

# EMPLOYEES DATABASE HAS BEEN DOWNLOADED FROM BELOW GITHUB LINK:
# https://github.com/h8/employees-database
# ==================================================================================================


# Import the packages
# ==================================================================================================
from sys import argv
from time import sleep
from sqlalchemy import create_engine
from llama_index.core import Settings
from origamibot import OrigamiBot as Bot
from llama_index.core import SQLDatabase
from origamibot.listener import Listener
from llama_index.llms.huggingface import HuggingFaceLLM
from llama_index.core.query_engine import NLSQLTableQueryEngine
from llama_index.embeddings.huggingface import HuggingFaceEmbedding

# ==================================================================================================


# Define LLM to be used later. Here we will use opensource llm and not ChatGPT.
# ==================================================================================================
hf_llm = HuggingFaceLLM(
    context_window=2048,  # tokens to be considered around each token to get the context
    max_new_tokens=256,  # new token generation length, especially for text generation
    generate_kwargs={"temperature": 0.1, "do_sample": True},  # generation parameters
    tokenizer_name="databricks/dolly-v2-3b",  # tokenizer used
    model_name="databricks/dolly-v2-3b",  # model used
    device_map="auto",  # device mapping (auto selects device automatically)
    # tokenizer configuration-specifying the maximum length of the input sequence the tokenizer can handle.
    # cache_dir for downloading the data to specified directory.
    tokenizer_kwargs={
        "cache_dir": "D:\\Repository\\AI\\LlamaIndex\\model_data",
        "max_length": 2048,
    },
    # cache_dir for downloading the data to specified directory.
    model_kwargs={
        "cache_dir": "D:\\Repository\\AI\\LlamaIndex\\model_data",
        "offload_folder": "D:\\Repository\\AI\\LlamaIndex\\model_data",
    },
    # uncomment this if using CUDA to reduce memory usage
    # model_kwargs={"torch_dtype": torch.float16}   # model configuration for CUDA memory reduction
)


# Define embeddings model as well
# ..............................................................................
# for device: Expected one of cpu, cuda, ipu, xpu, mkldnn, opengl, opencl, ideep, hip, ve, fpga, ort, xla, lazy, vulkan, mps, meta, hpu, mtia
embed_model = HuggingFaceEmbedding(
    model_name="sentence-transformers/bert-base-nli-mean-tokens",
    cache_folder="D:\\Repository\\AI\\LlamaIndex\\model_data",
    device="cpu",
)


# Setting LLM and Embed Model at global level.
# ..............................................................................
Settings.llm = hf_llm
Settings.embed_model = embed_model
# ==================================================================================================


# Define postgres database connection and other variables
# ==================================================================================================
# POSTGRES DB Variables
# ..............................................................................
host = "localhost"
port = 5432
username = "postgres"
password = 123456
database_name = "employees_database"

# TO be used at later stage
# ..............................................................................
DATABASE_URL = (
    f"postgresql+psycopg2://{username}:{password}@{host}:{port}/{database_name}"
)
engine = create_engine(DATABASE_URL)
# ==================================================================================================

# Query single table only
# ==================================================================================================
# Define the tables we need to include to query
sql_database = SQLDatabase(engine, schema="employees", include_tables=["employee"])

# Define the query engine
query_engine_nlsql = NLSQLTableQueryEngine(
    sql_database=sql_database, tables=["employee"]
)
# ==================================================================================================


# telegram related stuff -----------------------------------------------------------------------------------------------
class BotsCommands:
    """
    This are the commands which you can use in chat like..........
    /start will start the conversation
    /echo will echo the message
    """

    def __init__(self, bot: Bot):  # Can initialize however you like
        self.bot = bot

    def start(self, message):  # /start command
        self.bot.send_message(message.chat.id, "Hello user!\nThis is an example bot.")

    def echo(self, message, value: str):  # /echo [value: str] command
        self.bot.send_message(message.chat.id, value)

    def _not_a_command(self):  # This method not considered a command
        print("I am not a command")


class MessageListener(Listener):  # Event listener must inherit Listener
    """
    This is the message listener. Based on the question this portion will give
    answer. This will be responsible for conversation with user.
    """

    def __init__(self, bot):
        self.bot = bot
        self.m_count = 0

    def on_message(self, message):  # called on every message
        self.m_count += 1
        print(f"Total messages: {self.m_count}")
        print(f"Query is: {message.text}")
        ans = query_engine_nlsql.query(message.text)
        self.bot.send_message(message.chat.id, ans.response)

    def on_command_failure(self, message, err=None):  # When command fails
        if err is None:
            self.bot.send_message(message.chat.id, "Command failed to bind arguments!")
        else:
            self.bot.send_message(message.chat.id, f"Error in command:\n{err}")


if __name__ == "__main__":
    token = argv[1] if len(argv) > 1 else input("Enter bot token: ")
    bot = Bot(token)  # Create instance of OrigamiBot class

    # Add an event listener
    bot.add_listener(MessageListener(bot))

    # Add a command holder
    bot.add_commands(BotsCommands(bot))

    # We can add as many command holders
    # and event listeners as we like

    bot.start()  # start bot's threads
    print("*" * 25)
    print("Bot has been started!!!")
    while True:
        sleep(1)
        # Can also do some useful work in main thread
        # Like autoposting to channels for example
