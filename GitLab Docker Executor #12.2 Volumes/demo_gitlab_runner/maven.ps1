# In case you face the error of script execution 
# not allowed on this system
# Run powershell as an admin

# Set-ExecutionPolicy RemoteSigned

# check java version
java --version

# conditional message
if($?){
    echo "Java has already been installed"
} else {
    echo "Please install Java"
}

mvn -v

if($?){
    echo "MVN is in path"
} else {
    echo "MVN is not in path"

	# download maven
	wget -o C:\apache-maven-3.8.1-bin.zip https://mirrors.estointernet.in/apache/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.zip

	# check download was sucessfull or not
	if($?){
		echo "Successful"
	} else {
		echo "Issue"
	}
	
	# unzip the zip file
	Expand-Archive -path "C:\apache-maven-3.8.1-bin.zip" -DestinationPath "C:\maven"

	# add to the path for powershell session
	# it will be removed on exit of the power shell
	# another solution is to add it manually or
	# run with different set of commands with admin rights
	$env:Path += ";C:\maven\apache-maven-3.8.1\bin"

}

mvn -v


