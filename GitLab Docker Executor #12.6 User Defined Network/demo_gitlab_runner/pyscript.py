import mysql.connector
import os
import time

# Need to compulsory wait for few mins
# So that MySQL service will start by that time
# If not wait in that case will result in an error
# i.e. mysql.connector.errors.InterfaceError: 2003: Can't connect to MySQL server
time.sleep(600)

mydb = mysql.connector.connect(host="mysql_test",user="nayan",password="nayan", database="python_test")
print(mydb)

# Creating cursor
# open a connection to the MySQL server and store the object
mysql_cursor = mydb.cursor()

# In case if we want we can create database by below command
# mysql_cursor.execute("CREATE DATABASE College")

# create table
mysql_cursor.execute("CREATE TABLE customers (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), address VARCHAR(255))")

#
print("Customers table has been created")


# Insert values into Customers table
sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = [("Nayan", "India"),
        ("Nayan1", "India"),
        ("Nayan2", "India"),
        ("Nayan3", "India"),
        ("Nayan4", "India")
      ]

# execute the sql query      
mysql_cursor.executemany(sql, val)

# commit the changes
mydb.commit()

print(mysql_cursor.rowcount, "was inserted.")

# Just to check on mysql container, does it contains the data or not
time.sleep(1800)