"""
This script demonstrate how to achieve Retrieval Augmented Generation [RAG]
with keras_nlp package. In this script, I have shown how to do RAG without any
3rd party package.

In RAG, we have 3 main components:
----------------------------------
Vector Embedding or Embedding:
    Vector [Numerical] representation of given text data.

Retriever:
    Based on vector embedding, retriver will retrive relevant documents/text for
    the given query.

Generator:
    Large Language Model [LLM]. It will generate the answer from retrived docs/texts.


Package Versions:
-----------------
numpy==1.26.4
keras_nlp==0.14.4


Reference:
----------
https://keras.io/api/keras_nlp/models/gpt2/gpt2_causal_lm/
"""

# Import required packages
# ======================================================================================================================
import keras_nlp
import numpy as np


# Load models for respective tasks
preprocessor = keras_nlp.models.GPT2CausalLMPreprocessor.from_preset("gpt2_extra_large_en")
backbone = keras_nlp.models.GPT2Backbone.from_preset("gpt2_extra_large_en")
model1 = keras_nlp.models.GPT2CausalLM.from_preset("gpt2_extra_large_en", preprocessor=None)
model2 = keras_nlp.models.GPT2CausalLM.from_preset("gpt2_extra_large_en")


# Prepare the data
# ======================================================================================================================
# Raw data
sentences = [
    "The silver kite danced across the azure sky as the wind whispered secrets to the trees.",
    "Beneath the crimson glow of the lantern, the shadows of the ancient statues seemed to come alive.",
    "With each step, the mossy ground beneath his boots released the earthy scent of pine and damp soil.",
    "A melody of forgotten dreams lingered in the quiet corners of the deserted library.",
    "As the first snowflakes touched the frozen lake, the silence of winter embraced the valley.",
    "The clocktower chimed midnight, its echoes intertwining with the distant howl of the wolves.",
    "She found solace in the rhythmic ticking of the antique clock on the mantle.",
    "The rusty gate creaked open, revealing a hidden garden overgrown with wildflowers and ivy.",
    "His laughter resonated through the narrow alleys, scattering the pigeons perched on the old brick walls.",
    "The abandoned lighthouse stood tall against the stormy sea, its light flickering like a heartbeat.",
    "A single feather drifted down from the oak tree, landing softly on the sunlit grass.",
    "The scent of freshly baked bread wafted through the cobblestone streets, drawing people to the small bakery.",
    "Raindrops tapped gently against the windowpane, creating a soothing rhythm in the quiet room.",
    "The forgotten diary lay hidden under a pile of dusty books in the attic.",
    "A butterfly landed on her outstretched hand, its wings fluttering gently in the breeze.",
    "The old wooden bridge groaned under the weight of his steps as he crossed the river.",
    "Stars began to peek out from the twilight sky, casting a soft glow over the sleepy town.",
    "The echo of footsteps in the empty hallway sent a shiver down her spine.",
    "A small stream trickled through the forest, its waters sparkling in the dappled sunlight.",
    "The scent of lavender filled the air, calming her racing thoughts.",
    "A single candle flickered in the dark room, casting long shadows on the walls.",
    "The distant sound of a train whistle broke the silence of the night.",
    "Leaves rustled gently as a cool breeze swept through the autumn forest.",
    "The old typewriter clicked and clacked as he typed out the final chapter of his novel.",
    "The scent of rain lingered in the air long after the storm had passed."
]


# Define required funtions
# ======================================================================================================================
# Define a function to generate embeddings
def get_embeddings(text: str) -> np.ndarray:
    """
    To generate vector embeddings of the given data.

    Parameters
    ----------
    text: str
        Sentence
    
    Returns
    -------
    numpy array of vector embeddings
    """
    # Preprocess data and get embeddings through backbone.
    # Preprocess will contain tokenization as well.
    processed_text = preprocessor(text)[0]

    # Need to do reshape else backbone will raise shape error
    processed_text["token_ids"] = processed_text["token_ids"].numpy().reshape(1,-1)
    processed_text["padding_mask"] = processed_text["padding_mask"].numpy().reshape(1,-1)

    # Backbone will provide embeddings
    embeddings = backbone(processed_text)

    return embeddings


# We will use cosine similarity to find nearest docs/sentences related to the query
def cosine_similarity(embedding_a, embedding_b):
    """
    A function which implements cosine similarity calculation between 2 embeddings.
    First will be embeddings from query and second from our raw data.
    """
    # Need to transpose any of the embeddings numpy array. If we wont it will
    # raise an error related to multiplication.
    dot_product = np.dot(embedding_a.numpy(), embedding_b.numpy().transpose(0, 2, 1))
    norm_a = np.linalg.norm(embedding_a)
    norm_b = np.linalg.norm(embedding_b)
    return dot_product / (norm_a * norm_b)


# Get relevant docs
def retrieve_documents(query: str, top_k : int=1):
    """
    Function to get top n relevant documents for the given query.
    For the given query generate embedings of the query,
    then find similar sentences/docs
    then sort them and return
    """
    # Generate embeddings for the given query
    query_embedding = get_embeddings(query)

    similarities = {}
    for doc_id, embedding in document_embeddings.items():

        # Using query embeddings and raw data embedings get most relevant docs.
        similarity = cosine_similarity(query_embedding, embedding)

        # Reduce to a scalar (e.g., taking the mean similarity)
        similarity = np.mean(similarity)
        similarities[doc_id] = similarity

    # Sort documents by similarity
    sorted_docs = sorted(similarities.items(), key=lambda x: x[1], reverse=True)
    return sorted_docs[:top_k]


# Next we will generate answer based on the docs return by above function.
def generate_answer(query, method=1):
    """
    Generate the answer either based on tokens or based on retrieved docs.
    """
    relevant_docs = retrieve_documents(query)
    context = " ".join([sentences[doc_id] for doc_id, _ in relevant_docs])

    # input_text = query + " " + context
    input_text = f"""
                    Context: {context}
                    Question: {query}
                    Answer:
                """
    
    print("Input text is:", input_text)
    print("+"*25)

    if method==1:
        """
        Using preprocessor which will provide token_ids.
        
        Using these token_ids we will generate new token_ids and then we will
        detokenize them to get human readable answer.
        """
        tokenized_input = preprocessor(input_text)[0]
        tokenized_input["token_ids"] = np.array([tokenized_input["token_ids"].numpy().tolist()])
        tokenized_input["padding_mask"] = np.array([tokenized_input["padding_mask"].numpy().tolist()])

        # Generate text based on given context
        generated_output = model1.generate(tokenized_input, stop_token_ids=None, max_length=250)

        # Detokenize the text
        generated_output = preprocessor.tokenizer.detokenize(generated_output["token_ids"])

    elif method==2:
        """
        In this method, we will find similar docs related to given query using
        vector embeddings. These are the vector embeddings generated by backbone.
        
        We will input these retrived docs to generate the answer.
        """
        generated_output = model2.generate(input_text, max_length=500)

    return generated_output


# Generate embeddings and store it
# ======================================================================================================================
# Store embeddings in a dictionary as we are not using 3rd party vector store.
document_embeddings = {}
for doc_id, text in enumerate(sentences):
    embeddings = get_embeddings(text)
    document_embeddings[doc_id] = embeddings


# Finally generate the answer
# ======================================================================================================================
query = "Where is silver kite dancing?"

print("*"*108)
print("Method 1")
answer1 = generate_answer(query, method=1)
print(answer1)

print("*"*108)
print("Method 2")
answer2 = generate_answer(query, method=2)
print(answer2)
