"""
This script demonstrate how to achieve Retrieval Augmented Generation [RAG]
with keras_nlp package. In this script, I have shown how to do RAG with ChromaDB
package where ChromDB is a vector store which will store vectore embeddings.

In RAG, we have 3 main components:
----------------------------------
Vector Embedding or Embedding:
    Vector [Numerical] representation of given text data.
    Need to use LLM or Embedding model to generate this.

Retriver:
    Based on vector embedding, retriver will retrive relevant documents/text for
    the given query.
    We will use ChromaDB to store the embeddings and find relevant doc/text.

Generator:
    Large Language Model [LLM]. It will generate the answer from retrived docs/texts.


Package Versions:
-----------------
numpy==1.26.4
keras_nlp==0.14.4
chromadb==0.5.5


Reference:
----------
https://keras.io/api/keras_nlp/models/gpt2/gpt2_causal_lm/
"""

# Import required packages
# ======================================================================================================================
import chromadb
import keras_nlp
import numpy as np
from chromadb.config import Settings


# Load models for respective tasks
preprocessor = keras_nlp.models.GPT2CausalLMPreprocessor.from_preset("gpt2_extra_large_en")
backbone = keras_nlp.models.GPT2Backbone.from_preset("gpt2_extra_large_en")
model1 = keras_nlp.models.GPT2CausalLM.from_preset("gpt2_extra_large_en", preprocessor=None)
model2 = keras_nlp.models.GPT2CausalLM.from_preset("gpt2_extra_large_en")


# Prepare the data
# ======================================================================================================================
# Consider each senetence as a new doc
sentences = [
    "The silver kite danced across the azure sky as the wind whispered secrets to the trees.",
    "Beneath the crimson glow of the lantern, the shadows of the ancient statues seemed to come alive.",
    "With each step, the mossy ground beneath his boots released the earthy scent of pine and damp soil.",
    "A melody of forgotten dreams lingered in the quiet corners of the deserted library.",
    "As the first snowflakes touched the frozen lake, the silence of winter embraced the valley.",
    "The clocktower chimed midnight, its echoes intertwining with the distant howl of the wolves.",
    "She found solace in the rhythmic ticking of the antique clock on the mantle.",
    "The rusty gate creaked open, revealing a hidden garden overgrown with wildflowers and ivy.",
    "His laughter resonated through the narrow alleys, scattering the pigeons perched on the old brick walls.",
    "The abandoned lighthouse stood tall against the stormy sea, its light flickering like a heartbeat.",
    "A single feather drifted down from the oak tree, landing softly on the sunlit grass.",
    "The scent of freshly baked bread wafted through the cobblestone streets, drawing people to the small bakery.",
    "Raindrops tapped gently against the windowpane, creating a soothing rhythm in the quiet room.",
    "The forgotten diary lay hidden under a pile of dusty books in the attic.",
    "A butterfly landed on her outstretched hand, its wings fluttering gently in the breeze.",
    "The old wooden bridge groaned under the weight of his steps as he crossed the river.",
    "Stars began to peek out from the twilight sky, casting a soft glow over the sleepy town.",
    "The echo of footsteps in the empty hallway sent a shiver down her spine.",
    "A small stream trickled through the forest, its waters sparkling in the dappled sunlight.",
    "The scent of lavender filled the air, calming her racing thoughts.",
    "A single candle flickered in the dark room, casting long shadows on the walls.",
    "The distant sound of a train whistle broke the silence of the night.",
    "Leaves rustled gently as a cool breeze swept through the autumn forest.",
    "The old typewriter clicked and clacked as he typed out the final chapter of his novel.",
    "The scent of rain lingered in the air long after the storm had passed."
]


# Define required funtions
# ======================================================================================================================
# Define a function to generate embeddings
def get_embeddings(text: str) -> np.ndarray:
    """
    To generate vector embeddings of the given data.

    Parameters
    ----------
    text: str
        Sentence

    Returns
    -------
    numpy array [batch size, sequence length, embedding size]   # if using backbone output
    numpy array [batch size, embedding size]    # if taking mean along axis=1
    """
    # Preprocess data and get embeddings through backbone.
    # Preprocess will contain tokenization as well.
    # Fetching dict only
    processed_text = preprocessor(text)[0]

    # Need to do reshape else backbone will raise shape error
    processed_text["token_ids"] = processed_text["token_ids"].numpy().reshape(1,-1)
    processed_text["padding_mask"] = processed_text["padding_mask"].numpy().reshape(1,-1)

    embeddings = backbone(processed_text)

    # mean of the sequence axis to get a 2D array to be used later on for ChromaDB
    # shape will become [batch size, embedding size]
    embeddings = np.mean(embeddings, axis=1)

    return embeddings


# For the given query generate embedings of the query,
# then find similar sentences/docs
# then sort them and return
def retrieve_documents(query: str, top_k: int=1) -> list[str]:
    """
    To retrive relevant document which matches the query. The retrieval will be
    based on vector embeddings.

    Parameters
    ----------
    query: str
        Sentence
    
    top_k: int (Default=1)
        How many relevant docs to fetch?

    Returns
    -------
    list[str]
        List of docs which are matching with the given query based on vec embeddings
    """
    # Load the stored ChromaDB index
    # As we are in the same script not using this functionality
    # client = chromadb.Client(Settings())
    # OR if the file is at different location then
    # client = chromadb.Client(settings={"database_path": "path/to/chroma_db.sqlite"})
    # collection = client.get_collection(name="document_embeddings")

    query_embedding = get_embeddings(query)

    # Query the collection
    results = collection.query(
        query_embeddings=query_embedding,
        n_results=top_k
    )

    # Extract the document texts from the results
    retrieved_docs = [result["document"] for result in results["metadatas"][0]]
    return retrieved_docs


# Next we will generate answer based on the docs return by above function.
def generate_answer(query: str, method: int=1) -> str:
    """
    To retrive relevant document which matches the query. The retrieval will be
    based on vector embeddings.

    Parameters
    ----------
    query: str
        Sentence
    
    method: int (Default=1)
        Method to be used to generate the answer.
        - In first method we are not using preprocessor. We will provide preprocessor
        to generate method and detokenize it to get human redable answer.
        - In second method we will use retrieved docs to generate the answer.

    Returns
    -------
    str
        Generated answer
    """
    # Retrieve docs related to query
    relevant_docs = retrieve_documents(query)

    context = " ".join([sen for sen in relevant_docs])
    # input_text = query + " " + context
    input_text = f"""
                    Context: {context}
                    Question: {query}
                    Answer:
                """
    
    print("Input text is:", input_text)
    print("+"*25)

    if method==1:
        """
        Using preprocessor which will provide token_ids instead of embeddings.
        Using these token_ids we will generate new token_ids and then we will
        detokenize them to get answer.
        """
        # Preprocessor output will be tuple in which first element is useful hence extract it
        tokenized_input = preprocessor(input_text)[0]

        # Need to provide in specific format to generate method, hence changing the format here
        tokenized_input["token_ids"] = np.array([tokenized_input["token_ids"].numpy().tolist()])
        tokenized_input["padding_mask"] = np.array([tokenized_input["padding_mask"].numpy().tolist()])

        # Generate text based on given context
        generated_output = model1.generate(tokenized_input, stop_token_ids=None, max_length=250)

        # Detokenize the text to generate human readable text
        generated_output = preprocessor.tokenizer.detokenize(generated_output["token_ids"])

    elif method==2:
        """
        In this method, we will find similar docs related to given query using
        vector embeddings. These vector embeddings will be generated by backbone.
        
        We will input these retrived docs to generate the answer.
        """
        generated_output = model2.generate(input_text, max_length=500)

    return generated_output


# Embeddings
# ======================================================================================================================
# Initialize ChromaDB client
client = chromadb.Client(Settings())

# Create or connect to a collection
collection = client.get_or_create_collection(name="document_embeddings")

# Generate embeddings for given docs
document_embeddings = [get_embeddings(doc).tolist() for doc in sentences]

# Add embeddings to ChromaDB
for i, embedding in enumerate(document_embeddings):
    collection.add(
        ids=[str(i)],
        embeddings=embedding,
        metadatas=[{"document": sentences[i]}] 
    )


# Finally generate the answer
# ======================================================================================================================
query = "Where is silver kite dancing?"

print("*"*108)
print("Method 1")
answer1 = generate_answer(query, method=1)
print(answer1)

print("*"*108)
print("Method 2")
answer2 = generate_answer(query, method=2)
print(answer2)
