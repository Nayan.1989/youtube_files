"""
This script demonstarte basics of keras_nlp components and usage of methods
with and without .from_preset()

This script is solely for text generation purpose.

For private resources it wont work. It will require authentication.

Run the code in Python 3.11.
As per the below URL it wont run on windows.
https://github.com/tensorflow/text#a-note-about-different-operating-system-packages
On Fedora and Ubuntu having Python 3.12 its not running as well.

2 Ways through which we can call the models.
https://keras.io/api/keras_nlp/base_classes/causal_lm/#:~:text=This%20constructor%20can%20be%20called%20in%20one%20of%20two%20ways

Preset can be passed by any of the 4 ways.
https://keras.io/api/keras_nlp/base_classes/causal_lm/#:~:text=The%20preset%20can%20be%20passed%20as%20a%20one%20of

Architecture
https://keras.io/guides/keras_nlp/getting_started/#:~:text=Here%20is%20the%20modular%20hierarchy%20for%20BertClassifier%20(all%20relationships%20are%20compositional)%3A
"""
# Import required packages
# ------------------------------------------------------------------------------
import os

# Need to setup this environment variable first
# ------------------------------------------------------------------------------
os.environ["KERAS_BACKEND"] = "jax"  # or "tensorflow" or "torch"

# Import remaining packages
# ------------------------------------------------------------------------------
import keras_nlp
import keras

# Use mixed precision to speed up all training.
# https://keras.io/api/mixed_precision/
# For most of the ops it will be float16 and for critical ops it will be float32
# where high accuracy is required.
keras.mixed_precision.set_global_policy("mixed_float16")


# Check what's inside models and what can we use?
# https://keras.io/api/keras_nlp/models/
# ------------------------------------------------------------------------------
dir(keras_nlp.models)
"""
OUTPUT:
['AlbertBackbone', 'AlbertClassifier', 'AlbertMaskedLM', 'AlbertMaskedLMPreprocessor',
'AlbertPreprocessor', 'AlbertTokenizer', 'Backbone', 'BartBackbone', 'BartPreprocessor',
'BartSeq2SeqLM', 'BartSeq2SeqLMPreprocessor', 'BartTokenizer', 'BertBackbone',
'BertClassifier', 'BertMaskedLM', 'BertMaskedLMPreprocessor', 'BertPreprocessor',
'BertTokenizer', 'BloomBackbone', 'BloomCausalLM', 'BloomCausalLMPreprocessor',
'BloomPreprocessor', 'BloomTokenizer', 'CausalLM', 'Classifier', 'DebertaV3Backbone',
'DebertaV3Classifier', 'DebertaV3MaskedLM', 'DebertaV3MaskedLMPreprocessor',
'DebertaV3Preprocessor', 'DebertaV3Tokenizer', 'DistilBertBackbone', 'DistilBertClassifier',
'DistilBertMaskedLM', 'DistilBertMaskedLMPreprocessor', 'DistilBertPreprocessor',
'DistilBertTokenizer', 'ElectraBackbone', 'ElectraPreprocessor', 'ElectraTokenizer',
'FNetBackbone', 'FNetClassifier', 'FNetMaskedLM', 'FNetMaskedLMPreprocessor',
'FNetPreprocessor', 'FNetTokenizer', 'FalconBackbone', 'FalconCausalLM',
'FalconCausalLMPreprocessor', 'FalconPreprocessor', 'FalconTokenizer', 'GPT2Backbone',
'GPT2CausalLM', 'GPT2CausalLMPreprocessor', 'GPT2Preprocessor', 'GPT2Tokenizer', 
'GPTNeoXBackbone', 'GPTNeoXCausalLM', 'GPTNeoXCausalLMPreprocessor', 'GPTNeoXPreprocessor',
'GPTNeoXTokenizer', 'GemmaBackbone', 'GemmaCausalLM', 'GemmaCausalLMPreprocessor',
'GemmaPreprocessor', 'GemmaTokenizer', 'Llama3Backbone', 'Llama3CausalLM',
'Llama3CausalLMPreprocessor', 'Llama3Preprocessor', 'Llama3Tokenizer', 'LlamaBackbone',
'LlamaCausalLM', 'LlamaCausalLMPreprocessor', 'LlamaPreprocessor', 'LlamaTokenizer',
'MaskedLM', 'MistralBackbone', 'MistralCausalLM', 'MistralCausalLMPreprocessor',
'MistralPreprocessor', 'MistralTokenizer', 'OPTBackbone', 'OPTCausalLM',
'OPTCausalLMPreprocessor', 'OPTPreprocessor', 'OPTTokenizer', 'PaliGemmaBackbone',
'PaliGemmaCausalLM', 'PaliGemmaCausalLMPreprocessor', 'PaliGemmaTokenizer', 'Phi3Backbone',
'Phi3CausalLM', 'Phi3CausalLMPreprocessor', 'Phi3Preprocessor', 'Phi3Tokenizer',
'Preprocessor', 'RobertaBackbone', 'RobertaClassifier', 'RobertaMaskedLM',
'RobertaMaskedLMPreprocessor', 'RobertaPreprocessor', 'RobertaTokenizer', 'Seq2SeqLM',
'T5Backbone', 'T5Tokenizer', 'Task', 'Tokenizer', 'WhisperAudioFeatureExtractor',
'WhisperBackbone', 'WhisperPreprocessor', 'WhisperTokenizer', 'XLMRobertaBackbone',
'XLMRobertaClassifier', 'XLMRobertaMaskedLM', 'XLMRobertaMaskedLMPreprocessor',
'XLMRobertaPreprocessor', 'XLMRobertaTokenizer', 'XLNetBackbone', '__builtins__',
'__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__path__',
'__spec__']
"""


# ======================================================================================================================
# Methods with pre-configured configurations - Option 1
# ======================================================================================================================

# To check available presets
print(keras_nlp.models.GPT2Preprocessor.presets.keys())
"""
OUTPUT:
dict_keys(['gpt2_base_en', 'gpt2_medium_en', 'gpt2_large_en', 'gpt2_extra_large_en',
'gpt2_base_en_cnn_dailymail'])
"""

print(keras_nlp.models.GPT2CausalLM.presets.keys())
"""
OUTPUT:
dict_keys(['gpt2_base_en', 'gpt2_medium_en', 'gpt2_large_en', 'gpt2_extra_large_en',
'gpt2_base_en_cnn_dailymail'])
"""

tokenizer1 = keras_nlp.models.GPT2Tokenizer.from_preset("gpt2_base_en")

# This will contain tokenizer inherited from the given preset
preprocessor1 = keras_nlp.models.GPT2CausalLMPreprocessor.from_preset("gpt2_base_en")

backbone1 = keras_nlp.models.GPT2Backbone.from_preset("gpt2_base_en")

# This will contain backbone and preprocessor inherited from the given preset
llm1 = keras_nlp.models.GPT2CausalLM.from_preset("gpt2_base_en")



# ======================================================================================================================
# Methods with pre-configured configurations - Option 2
# ======================================================================================================================
tokenizer2 = keras_nlp.models.Tokenizer.from_preset("gpt2_base_en")

# This will contain tokenizer inherited from the given preset
# We can't call it using below way.
# preprocessor2 = keras_nlp.models.Preprocessor.from_preset("gpt2_base_en")
# Hence
preprocessor2 = preprocessor1

backbone2 = keras_nlp.models.Backbone.from_preset("gpt2_base_en")

# This will contain backbone and preprocessor inherited from the given preset
llm2 = keras_nlp.models.CausalLM.from_preset("gpt2_base_en")



# ======================================================================================================================
# Methods without pre-configured configurations i.e. with custom configs.
# ======================================================================================================================

# tokenizer3 = keras_nlp.models.GPT2Tokenizer(
#     vocabulary=vocab,
#     merges=merges,
# )

# using preset tokenizer as we dont have vocab and merges
tokenizer3= tokenizer1


preprocessor3 = keras_nlp.models.GPT2CausalLMPreprocessor(
    tokenizer=tokenizer3,
    sequence_length=128,
)
backbone3 = keras_nlp.models.GPT2Backbone(
    vocabulary_size=30552,
    num_layers=4,
    num_heads=4,
    hidden_dim=256,
    intermediate_dim=512,
    max_sequence_length=128,
)

llm3 = keras_nlp.models.GPT2CausalLM(
        backbone=backbone3,
        preprocessor=preprocessor3,
    )



# Call all tokenizers
# --------------------------------------------------------------------------------------------------
tokenizer1("Explain LLMs in simple terms.")
tokenizer2("Explain LLMs in simple terms.")
tokenizer3("Explain LLMs in simple terms.")
"""
OUTPUT:
# NOTE: It will be same as we are using same tokenizer
Array([18438,   391, 27140, 10128,   287,  2829,  2846,    13], dtype=int32)
"""


# Call all preprocessors
# --------------------------------------------------------------------------------------------------
preprocessor1("Explain LLMs in simple terms.")
preprocessor2("Explain LLMs in simple terms.")
"""
OUTPUT:
# NOTE: It will be same as we are using same preprocessor
({'token_ids': Array([50256, 18438,   391, ...,     0,     0,     0], dtype=int32),
'padding_mask': Array([ True,  True,  True, ..., False, False, False], dtype=bool)},
Array([18438,   391, 27140, ...,     0,     0,     0], dtype=int32), Array([ True,
 True,  True, ..., False, False, False], dtype=bool))
"""

preprocessor3("Explain LLMs in simple terms.")
"""
OUTPUT:
({'token_ids': Array([50256, 18438,   391, 27140, 10128,   287,  2829,  2846,    13,
       50256,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0], dtype=int32), 'padding_mask': Array([ True,  True,  True,
        True,  True,  True,  True,  True,  True,
        True, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False], dtype=bool)}, Array([18438,   391, 27140, 10128,   287,
       2829,  2846,    13, 50256,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0,     0,     0,     0,     0,     0,     0,     0,
           0,     0], dtype=int32), Array([ True,  True,  True,  True,  True,
           True,  True,  True,  True,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False, False, False, False, False, False, False, False,
       False, False], dtype=bool))
"""



# Call all backbones
# --------------------------------------------------------------------------------------------------
pr1 = preprocessor1("Explain LLMs in simple terms.")[0]
pr1["token_ids"] = pr1["token_ids"].reshape(1,-1)
pr1["padding_mask"] = pr1["padding_mask"].reshape(1,-1)
backbone1(pr1)
"""
OUTPUT:
Array([[[-0.0671  ,  0.0883  , -0.3071  , ...,  0.029   ,  0.05093 ,
         -0.001824],
        [-0.644   ,  0.5547  , -0.02611 , ..., -0.6855  , -0.06158 ,
         -0.03574 ],
        [-0.366   , -0.3328  , -1.357   , ..., -0.078   ,  0.877   ,
          0.4165  ],
        ...,
        [ 0.2534  , -0.3137  ,  0.04077 , ...,  0.051   , -0.1796  ,
         -0.114   ],
        [ 0.2522  , -0.3157  ,  0.04663 , ...,  0.05045 , -0.1833  ,
         -0.1163  ],
        [ 0.2249  , -0.1868  ,  0.6743  , ...,  0.1254  , -0.0435  ,
         -0.08167 ]]], dtype=float16)
"""


pr2 = preprocessor2("Explain LLMs in simple terms.")[0]
pr2["token_ids"] = pr2["token_ids"].reshape(1,-1)
pr2["padding_mask"] = pr2["padding_mask"].reshape(1,-1)
backbone2(pr2)
"""
OUTPUT:
Array([[[-0.0671  ,  0.0883  , -0.3071  , ...,  0.029   ,  0.05093 ,
         -0.001824],
        [-0.644   ,  0.5547  , -0.02611 , ..., -0.6855  , -0.06158 ,
         -0.03574 ],
        [-0.366   , -0.3328  , -1.357   , ..., -0.078   ,  0.877   ,
          0.4165  ],
        ...,
        [ 0.2534  , -0.3137  ,  0.04077 , ...,  0.051   , -0.1796  ,
         -0.114   ],
        [ 0.2522  , -0.3157  ,  0.04663 , ...,  0.05045 , -0.1833  ,
         -0.1163  ],
        [ 0.2249  , -0.1868  ,  0.6743  , ...,  0.1254  , -0.0435  ,
         -0.08167 ]]], dtype=float16)
"""


pr3 = preprocessor3("Explain LLMs in simple terms.")[0]
pr3["token_ids"] = pr3["token_ids"].reshape(1,-1)
pr3["padding_mask"] = pr3["padding_mask"].reshape(1,-1)
backbone3(pr3)
"""
OUTPUT:
Array([[[nan, nan, nan, ..., nan, nan, nan],
        [nan, nan, nan, ..., nan, nan, nan],
        [nan, nan, nan, ..., nan, nan, nan],
        ...,
        [nan, nan, nan, ..., nan, nan, nan],
        [nan, nan, nan, ..., nan, nan, nan],
        [nan, nan, nan, ..., nan, nan, nan]]], dtype=float16)
"""


# Call all LLMs
# --------------------------------------------------------------------------------------------------
output1 = llm1.generate("Explain LLMs in simple terms.", max_length=100)
print(f"Output is:\n{output1}")
"""
OUTPUT:
Explain LLMs in simple terms. LLM (Linear Programming Language) is a language for
building and debugging programs in LLVM or LLVM-like code. LLVM is the standard
LLVM compiler that runs on all of the platforms, including Windows and Mac. The
LLVM language is a set of LLVM modules that can be used by a program to run in a
variety of ways, including:

Programs written in C, C++, C# and Objective-
"""


output2 = llm2.generate("Explain LLMs in simple terms.", max_length=100)
print(f"Output is:\n{output2}")
"""
OUTPUT:
Explain LLMs in simple terms.

The first step is to understand why LLMs are so common: They are a form of data
manipulation designed to manipulate data and to be manipulated with a high degree
of accuracy.

The second step is the most important: understand how data is manipulated.

The data manipulation process takes a number of steps:

The first step is to create a new dataset, which will be the data that the data
manipulations will be made from
"""


output3 = llm3.generate("Explain LLMs in simple terms.", max_length=100)
print(f"Output is:\n{output3}")
"""
OUTPUT:
Explain LLMs in simple terms.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""

