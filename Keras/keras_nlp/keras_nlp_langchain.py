"""
This script demonstrate how to achieve Retrieval Augmented Generation [RAG] by
integrating keras_nlp package with langchain and hugging face.

In this script we will use llm from keras_nlp and integrate it with langchain
using its LLM class. To load and process data we will use different functionalities
of langchain. To generate vector emebddings we will use model from huggingface.

In RAG, we have 3 main components:
----------------------------------
Vector Embedding or Embedding:
    Vector [Numerical] representation of given text data.
    Need to use LLM or Embedding model to generate this.

Retriver:
    Based on vector embedding, retriver will retrive relevant documents/text for
    the given query.

Generator:
    Large Language Model [LLM]. It will generate the answer from retrived docs/texts.


Package Versions:
-----------------
python==3.11.9
keras_nlp==0.14.4
tf-keras==2.17.0
langchain==0.3.4
langchain-community==0.3.3
faiss-cpu==1.9.0
sentence-transformers==3.2.0


Reference:
----------
https://python.langchain.com/docs/how_to/custom_llm/
"""

# Import required packages
# ======================================================================================================================
import keras_nlp
from langchain.llms.base import LLM
from langchain.schema import Document
from langchain.chains import RetrievalQA
from typing import Any, Iterator, List, Optional
from langchain_core.outputs import GenerationChunk
from langchain_community.vectorstores import FAISS
from langchain_community.document_loaders import DirectoryLoader
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_core.callbacks.manager import CallbackManagerForLLMRun


# Define global vaiables
# ======================================================================================================================

# Define keras_nlp model to be used with Langchain
# .............................................................................
gpt2 = keras_nlp.models.GPT2CausalLM.from_preset("gpt2_extra_large_en")


# Define required class and functions
# ======================================================================================================================
class GPT2LLM(LLM):
    """
    A class which overrides LLM class from Langchain.
    This LLM class is useful when we want to define custom llm. This custom
    llm can be private/local API or private/local model as well.
    """

    num_output: int = 128

    @property
    def _llm_type(self) -> str:
        """
        Get the type of language model used by this chat model.
        Used for logging purposes only
        """
        return "GPT2 Large Model"

    def _call(
        self,
        prompt: str,
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> str:
        """
        Run the LLM on the given input.

        Override this method to implement the LLM logic.

        Args:
            prompt: The prompt to generate from.
            stop: Stop words to use when generating. Model output is cut off at the
                first occurrence of any of the stop substrings.
                If stop tokens are not supported consider raising NotImplementedError.
            run_manager: Callback manager for the run.
            **kwargs: Arbitrary additional keyword arguments. These are usually passed
                to the model provider API call.

        Returns:
            The model output as a string.
        """
        print(f"{'*'*25} Prompt is \n {prompt}")

        response = gpt2.generate(prompt, max_length=self.num_output)

        return response

    def _stream(
        self,
        prompt: str,
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> Iterator[GenerationChunk]:
        """
        Stream the LLM on the given prompt.

        This method should be overridden by subclasses that support streaming.

        If not implemented, the default behavior of calls to stream will be to
        fallback to the non-streaming version of the model and return
        the output as a single chunk.

        Args:
            prompt: The prompt to generate from.
            stop: Stop words to use when generating. Model output is cut off at the
                first occurrence of any of these substrings.
            run_manager: Callback manager for the run.
            **kwargs: Arbitrary additional keyword arguments. These are usually passed
                to the model provider API call.

        Returns:
            An iterator of GenerationChunks.
        """
        response = ""
        for token in gpt2.generate(prompt, max_length=self.num_output):
            response += token
            yield response


# lang_gpt2 = GPT2LLM()

# print("Response generated by calling GPT2 model")
# print(gpt2.generate("What is artificial intelligence?"))

# print("Response generated by calling GPT2 model using Langchain CustomClass")
# print(lang_gpt2.complete("What is artifical intelligence?").text)


# Define global context for LLM and Embedings models
# ======================================================================================================================
llm = GPT2LLM()  # Using GPT2LLM as the language model
embed_model = HuggingFaceEmbeddings(model_name="sentence-transformers/all-MiniLM-L6-v2")
text_splitter = RecursiveCharacterTextSplitter(chunk_size=4096, chunk_overlap=100)


# Data Loading, Vector Embeddings Generation, Processing
# ======================================================================================================================
# Incase we want entire text file as single document we can use below code.
# documents = DirectoryLoader("/home/nayan/NLP/RAG/Scripts", glob="*.txt").load()

# Below code is to consider each sentence as separate document
# ..................................................................................................
# Read the text file and split into sentences
with open("/home/nayan/NLP/RAG/Scripts/sentences.txt", "r") as file:
    text = file.read()

# Assuming the sentences are separated by periods. Adjust as needed.
sentences = text.split(".")  # Split based on your preferred delimiter
print(type(sentences))  # It will be list data type

# Create Document objects for each sentence
documents = [
    Document(page_content=sentence) for sentence in sentences if sentence.strip()
]

# Split the text into chunks (if needed)
split_docs = text_splitter.split_documents(documents)
# ..................................................................................................

# Generate vector embedings using the model defined by Settings.embed_model
# Create the FAISS vector store and store embeddings
index = FAISS.from_documents(split_docs, embed_model)

# Save the index for persistence
index.save_local("/home/nayan/NLP/RAG/Scripts/index")


# Load index and generate the asnwer
# ======================================================================================================================
# load index
# index = FAISS.load_local("/home/nayan/NLP/RAG/Scripts/index", embed_model)

# Set up the query engine using LangChain's RetrievalQA chain
qa_chain = RetrievalQA.from_chain_type(
    llm=llm, chain_type="stuff", retriever=index.as_retriever()
)

# Ask questions and get responses
response_1 = qa_chain.invoke("Where is silver kite dancing?")
print("*" * 108)
print(response_1)
print("*" * 108)

# Out of context question
response_2 = qa_chain.invoke("What is artificial intelligence?")
print("*" * 108)
print(response_2)
print("*" * 108)
