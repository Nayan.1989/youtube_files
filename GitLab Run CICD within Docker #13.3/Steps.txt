# Steps ------------------------------------------------------------------------------------
1. Make sure docker service is running. Start it in case of service has been stopped.
	sudo systemctl status docker
	sudo systemctl start docker

	# Run the gitlab-runner image in a container called gitlab-runner if its not running already
	# If its running with that -p 8090:8090 option you can stop and remove the container using below command
	sudo docker stop gitlab-runner
	sudo docker rm gitlab-runner
	# Expose 8090 port of docker to host's 8090 port
	sudo docker ps -a # this is to check rather the container is running or not
	# if not then execute below command
	sudo docker run -d --name gitlab-runner -p 8090:8090 --restart always -v gitlab-runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner


2. From GitLab dashboard >  Project > Settings > CI/CD > Runners > Expand. Make sure here,
	the runner is showing as green which means it is accessible for any CICD pipeline.

3. Now whatever the CICD pipeline we are going to execute, it will be executed with gitlab-runner user i.e.,
	sudo docker exec -it -u gitlab-runner gitlab-runner bash

	# You can get the list of users using below command,
	cat /etc/passwd
	# You need to execute this command from the docker container or the system on which you want to know the users.

4. As we are using docker, it does not come with "sudo" hence first we need to install essential tools in the container.
	# We can install essential tools in the container using below command,
	sudo docker exec -it gitlab-runner bash -c "apt update && apt install sudo build-essential systemctl -y"

5. Next we need to set the root password, add gitlab-runner user to sudo group and set the password for the same, 
	for that we need to execute below command
	sudo docker exec -it gitlab-runner bash
	sudo passwd root
	usermod -aG sudo gitlab-runner
	passwd gitlab-runner

	# Next we need to add this gitlab-runner password to bashrc file of gitlab-runner user so that we dont need to
	# expose it. We need to do it in gitlab-runner user directory because if we have set it up in root user directory
	# then gitlab-runner user wont be able to access it.

	# Below command will create .bashrc file in gitalb-runner user's home directory if it does not exist.
	# If it exists then it will append the line at the end of the file.
	sudo docker exec -it -u gitlab-runner gitlab-runner bash
	echo 'password=abc123' >> ~/.bashrc
	source ~/.bashrc
	echo $password